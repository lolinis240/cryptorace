# Generated by Django 2.0 on 2018-02-17 10:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0002_auto_20180211_1445'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='balance',
            field=models.DecimalField(decimal_places=18, max_digits=24),
        ),
    ]
