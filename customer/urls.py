from django.urls import path

from django.contrib.auth import views as auth_views

from customer import views

app_name = 'customer'

urlpatterns = [

    # auth
    path('signup/', views.RegisterView.as_view(), name='register'),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='/'), name='logout'),

    path('profile/', views.ProfilePageView.as_view(), name='profile'),

    path('create_transaction/', views.CreateTransactionView.as_view(), name='create_transaction'),
    path('confirm_transaction/', views.confirm_transaction, name='confirm_transaction'),

    path('withdraw_request/', views.WithdrawRequestView.as_view(), name='withdraw_request'),
]
