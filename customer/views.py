import hmac

import constance
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.http import HttpResponse, JsonResponse
from django.contrib.auth import login, authenticate
from django.conf import settings
from django.views.generic import TemplateView, FormView

from customer.models import Transaction, WithdrawRequest
from customer.forms import CustomerRegistrationForm, CustomerLoginForm, WithdrawRequestForm, CreateTransactionForm, \
    CarSkinForm, AvatarForm
from customer.telegram_bot import send_message_to_dolfero


class LoginView(FormView):
    form_class = CustomerLoginForm
    http_method_names = ['post']

    def form_valid(self, form):
        user = authenticate(self.request,
                            username=form.cleaned_data['username'],
                            password=form.cleaned_data['password'])
        if user is not None:
            login(self.request, user)
            return JsonResponse({'status': 'ok'})
        else:
            return JsonResponse({'status': 'error',
                                 'message': 'Wrong username and/or password'}, status=422)

    def form_invalid(self, form):
        return JsonResponse({'status': 'error',
                             'errors': form.errors}, status=400)


class RegisterView(FormView):
    form_class = CustomerRegistrationForm
    success_url = '/'
    template_name = 'registration/signup.html'

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return super(RegisterView, self).form_valid(form)


class ProfilePageView(LoginRequiredMixin, TemplateView):
    template_name = 'customer/profile.html'
    form_class = WithdrawRequestForm
    success_url = reverse_lazy('customer:profile')

    def get_context_data(self, **kwargs):
        ctx = super(ProfilePageView, self).get_context_data(**kwargs)
        ctx['DEMO'] = settings.DEMO
        ctx['customer'] = self.request.user.customer
        ctx['forms'] = {
            'withdraw': WithdrawRequestForm(
                customer=self.request.user.customer,
                prefix='withdraw',
                data=self.request.POST if self.request.POST.get('action') == 'withdraw' else None),
            'car_skin': CarSkinForm(
                instance=self.request.user.customer,
                prefix='car_skin',
                data=self.request.POST if self.request.POST.get('action') == 'car_skin' else None),
            'avatar': AvatarForm(
                instance=self.request.user.customer,
                prefix='avatar',
                data=self.request.POST if self.request.POST.get('action') == 'avatar' else None,
                files=self.request.FILES if self.request.POST.get('action') == 'avatar' else None),
        }
        ctx['MINIMUM_DEPOSIT'] = constance.config.MINIMUM_DEPOSIT
        ctx['MINIMUM_WITHDRAW'] = constance.config.MINIMUM_WITHDRAW
        return ctx

    def post(self, request, *args, **kwargs):
        customer = request.user.customer

        ctx = self.get_context_data(**kwargs)
        if ctx['forms']['withdraw'].is_valid():
            with transaction.atomic():
                form = ctx['forms']['withdraw']
                withdraw_request = form.save(commit=False)
                withdraw_request.customer = customer
                withdraw_request.save()
                customer.balance -= form.cleaned_data['amount']
                customer.save()
                messages.success(request, 'Request was successfully sent')
                send_message_to_dolfero('''Withdraw request\n\nCustomer #{}\nAmount: {}\nAddress: {}'''.format(
                    customer.pk, form.cleaned_data['amount'], form.cleaned_data['address']
                ) + ('\n\n[DEMO]' if settings.DEMO else ''))
            return redirect('customer:profile')
        elif ctx['forms']['car_skin'].is_valid():
            ctx['forms']['car_skin'].save()
            messages.success(request, 'Car skin was successfully changed')
            return redirect('customer:profile')
        elif ctx['forms']['avatar'].is_valid():
            ctx['forms']['avatar'].save()
            messages.success(request, 'Avatar was successfully changed')
            return redirect('customer:profile')

        return self.render_to_response(ctx)


class WithdrawRequestView(LoginRequiredMixin, FormView):
    form_class = WithdrawRequestForm

    def form_valid(self, form):
        if form.cleaned_data['amount'] <= self.request.user.customer.balance:
            send_message_to_dolfero('''Withdraw request\n\nCustomer #{}\nAmount: {}\nAddress: {}'''.format(
                self.request.user.customer.pk, form.cleaned_data['amount'], form.cleaned_data['address']
            ))
            return JsonResponse({'status': 'ok'})
        else:
            return JsonResponse({'status': 'error',
                                 'errors': {'amount': 'Balance is not enough'}}, status=400)

    def form_invalid(self, form):
        return JsonResponse({'status': 'error',
                             'errors': form.errors}, status=400)


class CreateTransactionView(LoginRequiredMixin, FormView):
    form_class = CreateTransactionForm

    def form_valid(self, form):
        t = Transaction.objects.create(
            customer=self.request.user.customer,
            amount=form.cleaned_data['amount'],
        )
        if settings.DEMO:
            t.accept()
            messages.success(self.request, 'Funds were added to demo balance')
        return HttpResponse(str(t.pk), status=200)

    def form_invalid(self, form):
        return JsonResponse({'status': 'error',
                             'errors': form.errors}, status=400)


@require_http_methods(['POST'])
@csrf_exempt
def confirm_transaction(request):
    """
        Gettin POST from coinpayments and check transaction status
    """
    errors = []
    data = request.POST
    send_message_to_dolfero(str(data))

    if data.get('ipn_mode') != 'hmac':
        errors.append('IPN Mode is not HMAC')

    hmac_header = request.META.get('HTTP_HMAC')
    if not hmac_header:
        errors.append('No HMAC signature sent')

    if data.get('merchant') != settings.COINPAYMENTS_MERCHANT_ID:
        errors.append('Merchant ID is INVALID')

    hmac_value = hmac.new(settings.COINPAYMENTS_IPN_SECRET.encode(), request.body, 'sha512')
    if not hmac.compare_digest(hmac_value.hexdigest(), hmac_header):
        errors.append('Wrong HMAC signature')

    try:
        t = Transaction.objects.get(pk=data.get('invoice'))
    except Transaction.DoesNotExist:
        errors.append('Transaction "{}" was not found'.format(data.get('invoice')))

    if errors:
        send_message_to_dolfero('\n'.join(errors))
        return HttpResponse('WRONG SIGNATURE', status=400)

    try:
        status = int(data.get('status', 0))
        if status >= 100 or status == 2:
            t.accept()
    except ValueError:
        pass

    return HttpResponse('OK', status=200)
