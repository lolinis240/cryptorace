import constance
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.forms import ModelForm, Form, fields, widgets
from django.contrib.auth.models import User

from customer.models import WithdrawRequest, Customer


class CustomerRegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'email',
            'username',
            'password1',
            'password2',
        ]

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).count():
            raise ValidationError('Email already exists')
        return email

    def clean_username(self):
        username = self.cleaned_data['username']
        if User.objects.filter(username=username).count():
            raise ValidationError('Username already exists')
        return username


class CustomerLoginForm(Form):
    username = fields.CharField()
    password = fields.CharField(widget=widgets.PasswordInput)


class WithdrawRequestForm(ModelForm):
    def __init__(self, customer, **kwargs):
        super(WithdrawRequestForm, self).__init__(**kwargs)
        self.customer = customer
        self.fields['amount'].validators.append(MinValueValidator(constance.config.MINIMUM_WITHDRAW))

    def clean_amount(self):
        amount = self.cleaned_data['amount']
        if amount > self.customer.balance:
            raise ValidationError('Not enough funds')
        return amount

    class Meta:
        model = WithdrawRequest
        fields = 'address', 'amount'


class CarSkinForm(ModelForm):
    class Meta:
        model = Customer
        fields = "car_skin_val",


class AvatarForm(ModelForm):
    class Meta:
        model = Customer
        fields = 'avatar',


class CreateTransactionForm(Form):
    amount = fields.DecimalField(max_digits=24, decimal_places=18)

    def __init__(self, **kwargs):
        super(CreateTransactionForm, self).__init__(**kwargs)
        self.fields['amount'].validators.append(MinValueValidator(constance.config.MINIMUM_DEPOSIT))
