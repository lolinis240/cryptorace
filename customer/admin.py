from django.contrib import admin

from customer.models import Customer, Transaction, Currency, WithdrawRequest, CarSkin


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'username', 'first_name', 'last_name', 'balance')
    search_fields = ('user__email', 'user__username', 'user__first_name', 'user__last_name')
    readonly_fields = ('user',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'customer', 'amount', 'datetime', 'accepted')
    readonly_fields = ('customer', 'amount', 'accepted', 'datetime')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'price')
    readonly_fields = ('last_updated',)  # 'price')


@admin.register(WithdrawRequest)
class WithdrawRequestAdmin(admin.ModelAdmin):
    list_display = ('customer', 'address', 'amount', 'datetime', 'processed')
    readonly_fields = ('customer', 'address', 'amount', 'datetime')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(CarSkin)
class CarSkinAdmin(admin.ModelAdmin):
    pass
