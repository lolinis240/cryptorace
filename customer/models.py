from django.contrib.auth.models import User
from django.db import models, transaction
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.functional import cached_property


class CarSkin(models.Model):
    name = models.CharField(max_length=255)
    top_view = models.ImageField()
    side_view = models.ImageField(null=True)
    price = models.DecimalField(max_digits=24, decimal_places=18, default='0')

    def __str__(self):
        return self.name


class Customer(models.Model):
    """
    Customer.
    """

    balance = models.DecimalField(max_digits=24, decimal_places=18, default='0')
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='customer')
    car_skin_val = models.ForeignKey(CarSkin, on_delete=models.SET_NULL, null=True, blank=True, verbose_name='car skin')
    avatar = models.ImageField(default='no_avatar.png')

    @cached_property
    def car_skin(self):
        return self.car_skin_val or CarSkin.objects.first()

    @cached_property
    def email(self):
        return self.user.email

    @cached_property
    def username(self):
        return self.user.username

    @cached_property
    def first_name(self):
        return self.user.first_name

    @cached_property
    def last_name(self):
        return self.user.last_name

    def __str__(self):
        return self.user.email


class Transaction(models.Model):
    """
    Transaction - пополнение счета
    """

    accepted = models.BooleanField(default=False)
    amount = models.DecimalField(max_digits=24, decimal_places=18)
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT, related_name='transactions')
    datetime = models.DateTimeField(auto_now_add=True, verbose_name='Транзакция создана')

    @transaction.atomic
    def accept(self):
        """
        Make transaction accepted.
        """
        if self.accepted:
            return

        self.accepted = True
        self.customer.balance += self.amount
        self.save()
        self.customer.save()

    def __str__(self):
        return 'Transaction #{}'.format(self.pk)

    class Meta:
        verbose_name = 'Пополнение счета'
        verbose_name_plural = 'Пополнения счетов'


class Currency(models.Model):
    code = models.CharField(max_length=10)
    price = models.DecimalField(max_digits=24, decimal_places=18, verbose_name='Price in USD', null=True, blank=True)
    last_updated = models.DateTimeField(null=True, auto_now=True)

    def __str__(self):
        return self.code.upper()


class WithdrawRequest(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=24, decimal_places=18)
    address = models.CharField(max_length=42)
    processed = models.BooleanField(default=False)
    datetime = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '#{} ({})'.format(self.pk, self.customer)


@receiver(post_save, sender=User)
def user_created(sender, instance, created, **kwargs):
    if created:
        Customer.objects.create(user=instance)
