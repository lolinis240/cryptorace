var gulp = require('gulp');
    sass = require('gulp-sass');
    autoprefixer = require('gulp-autoprefixer');

gulp.task('default', ['watch']);

gulp.task('sass', function(){
    return gulp.src('static/scss/**/*.scss')
        .pipe(sass()).pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
        .pipe(gulp.dest('static/css'))
});

gulp.task('watch', ['sass'], function() {
    gulp.watch('static/scss/**/*.scss', ['sass']);
});
