$(document).ready(function(){

  var id = +$('.user-skin-id_js').text();
  var startSlide = 0;

  $('.slide').each(function(index) {
      if($(this).attr('data-skin-pk') == id){
        $('.slide').eq(index).addClass('check');
        startSlide = index
      }
  })

  $('#slider').slick({
    slidesToShow: 1,
    nextArrow: $('#next-a'),
    prevArrow: $('#prev-a'),
    swipe: false,
    initialSlide: startSlide,
  });


  $('#slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
    $('.blue-btn_js').prop('disabled',false);
    $('.blue-btn_js').text('FREE');
    var input = $('.slick-active').find('input');
    var cost = $('.slick-active').find('.cost').text();
    if(!input.prop('disabled'))
      input.click();
    else{
      $('.blue-btn_js').prop('disabled',true);
      $('.blue-btn_js').html('BUY - '+cost+'<img src="/static/img/eth.ico" width="30px">');
    }
  });

});
