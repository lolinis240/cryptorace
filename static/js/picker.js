
//var
var totalPercent = 0;
    maxValue = 100;
    isCorrectPercent = false;
var bodyReq = {};
var allSlider = {};



$(document).ready( function(){

  //set zero to all sliders
  $('.input-val-range-slider_js').val(0);
  $('#btn-submit-fuel-form_js').prop('disabled', true);

  //sliders
  $(".example").ionRangeSlider({
    min: 0,
    max: 100,
    from: 0,
    from_min: 0,

    //dynamical changes for user
    onChange: function (data) {
      var type = data.input[0].attributes.id.nodeValue;
      $('.'+type).val(data.from);
    },

  //dynamical changes on finish
    onFinish: function (data) {
      isCorrectPercent = false;
      totalPercent = 0;
      var type = data.input[0].attributes.id.nodeValue;
      $('.'+type).val(data.from);
      $('.input-val-range-slider_js').each(function(){
        totalPercent = totalPercent + parseInt($(this).val());
        if(totalPercent == 100){
          isCorrectPercent = true;
          $('#btn-submit-fuel-form_js').prop('disabled', false);
          $('#alert-fuel-form_js').addClass('isHidden');
        }
        else{
          $('#btn-submit-fuel-form_js').prop('disabled', true);
          $('#alert-fuel-form_js').removeClass('isHidden');
          $('#alert-fuel-form_js').text('Please choose exactly 100% ('+totalPercent+'%)');
        }
      })
    },

  });

// add all sliders to object
  $(".example").each(function(index){
    var type = $(this).attr('id');
    allSlider[type] = $(this).data("ionRangeSlider");
  });


});


  //change input without slider

  $(document).on('change paste keyup', '.input-val-range-slider_js', function(){
  isCorrectPercent = false;
  totalPercent = 0;
  $_this = $(this)
  var typeCoinSlider = 0;
  var trueValue = 0;
  $('.input-val-range-slider_js').each(function(){
    var i = parseInt($(this).val());
    if(!i){
      i=0;
      $(this).val(0);
    }
    if(i>100){
      i=100;
      $(this).val(100);
    }
    typeCoinSlider = $_this.attr('data-coin-name');
    trueValue = parseInt($_this.val())

    totalPercent = totalPercent + i;
    if(totalPercent == 100){
      isCorrectPercent = true;
      $('#btn-submit-fuel-form_js').prop('disabled', false);
      $('#alert-fuel-form_js').addClass('isHidden');
    }
    else{
      $('#btn-submit-fuel-form_js').prop('disabled', true);
      $('#alert-fuel-form_js').removeClass('isHidden');
      $('#alert-fuel-form_js').text('Please choose exactly 100% ('+totalPercent+'%)');
    }
  })

  //update value of slider
  allSlider[typeCoinSlider].update({
    from: trueValue,
  });
});


//check, make JSON and submit
$(document).on('click', '#btn-submit-fuel-form_js', function(e){
  e.preventDefault();
  $('.input-val-range-slider_js').each(function() {
    var type = $(this).attr('data-coin-name');
    var value = $(this).val();
    bodyReq[type] = ''+(value/100);
  })
  var jsonString = JSON.stringify(bodyReq);
  $('#id_percentages').val(jsonString);
  $('#join-form_js').submit();
});
