// https://github.com/uxitten/polyfill/blob/master/string.polyfill.js
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/padStart
if (!String.prototype.padStart) {
    String.prototype.padStart = function padStart(targetLength,padString) {
        targetLength = targetLength>>0; //floor if number or convert non-number to 0;
        padString = String(padString || ' ');
        if (this.length > targetLength) {
            return String(this);
        }
        else {
            targetLength = targetLength-this.length;
            if (targetLength > padString.length) {
                padString += padString.repeat(targetLength/padString.length); //append to original to ensure we are longer than needed
            }
            return padString.slice(0,targetLength) + String(this);
        }
    };
}

/*$(document).on('click', '.position_js', function () {
    $('.position_js').find('.hint').removeClass('active');
    $(this).find('.hint').addClass('active');
    var number = $(this).attr('id');
    $('.racer-info-block').removeClass('active');
    $('.' + number + '_js').addClass('active');
});*/


$(document).ready(function () {
    var template = new Ractive({
        target: '#template-target',
        template: '#race-watch-template',
        data: {
            data: raceData,
            ready: false,
            state: null,
            notStarted: false,
            ongoing: false,
            finished: false,
            skins: skins,
            timeLeft: '0:00:00',
            joinedCount: 0,
            selectedRacer: -1
        }
    });

    var loadState = function () {
        return $.get('/game/' + raceData.pk + '/state/').then(function (data) {
            if (data.racers_data.length && template.get('selectedRacer') === -1) {
                template.set({ selectedRacer: data.racers_data[0].racer.id })
            }
            template.set({
                notStarted: !data.started && !data.finished,
                ongoing: data.started && !data.finished,
                finished: data.started && data.finished,
                state: data,
                ready: true,
                joinedCount: data.racers_data.length
            });
        });
    };
    loadState();
    setInterval(loadState, 10000);

    var formatTime = function(diff) {
        var seconds = Math.round(diff % 60);
        var minutes = Math.floor(diff / 60) % 60;
        var hours = Math.floor(diff / 60 / 60);
        return String(hours) + ':' + String(minutes).padStart(2, '0') + ':' + String(seconds).padStart(2, '0');
    };

    var updateTime = function() {
        if (template.get('notStarted')) {
            template.set({
                timeLeft: formatTime(raceData.duration)
            });
        } else if (template.get('ongoing')) {
            var currentTime = Math.round(+new Date() / 1000);
            var diff = template.get('state.start_datetime') + raceData.duration - currentTime;
            if (diff <= 0) {
                diff = 0;
                loadState();
            }
            template.set({
                timeLeft: formatTime(diff)
            });
        } else {
            template.set({
                timeLeft: formatTime(0)
            });
        }
    };

    updateTime();
    setInterval(updateTime, 1000);
});
