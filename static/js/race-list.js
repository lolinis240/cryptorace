// https://github.com/uxitten/polyfill/blob/master/string.polyfill.js
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/padStart
if (!String.prototype.padStart) {
    String.prototype.padStart = function padStart(targetLength,padString) {
        targetLength = targetLength>>0; //floor if number or convert non-number to 0;
        padString = String(padString || ' ');
        if (this.length > targetLength) {
            return String(this);
        }
        else {
            targetLength = targetLength-this.length;
            if (targetLength > padString.length) {
                padString += padString.repeat(targetLength/padString.length); //append to original to ensure we are longer than needed
            }
            return padString.slice(0,targetLength) + String(this);
        }
    };
}

$(document).ready(function() {
  var formatTime = function(diff) {
      var seconds = Math.round(diff % 60);
      var minutes = Math.floor(diff / 60) % 60;
      var hours = Math.floor(diff / 60 / 60);
      return String(hours) + ':' + String(minutes).padStart(2, '0') + ':' + String(seconds).padStart(2, '0');
  };

  var $timers = $('.track-length_js'),
      $liveTimers = $timers.filter('[data-ongoing]');

  function updateTime() {
    $liveTimers.each(function(i, el) {
      el = $(el);
      var now = Math.round(+new Date() / 1000),
          duration = Number(el.data('duration')),
          start = Number(el.data('start'));

      var time = Math.max(start + duration - now, 0);
      el.text(formatTime(time));
    });
  }

  $timers.each(function(i, el) {
    if ($(el).data('ongoing') !== 'true') {
      $(el).text(formatTime(Number($(el).data('duration'))));
    }
  });

  updateTime();

  setInterval(updateTime, 500);
});
