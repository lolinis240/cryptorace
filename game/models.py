import uuid

from decimal import Decimal, getcontext
from django.contrib.postgres.fields import JSONField
from django.core import validators
from django.db import models, transaction
from django.utils import timezone
from django.utils.functional import cached_property

from customer.models import Customer, Currency

START_PORTFOLIO_SUM = 100000


class Track(models.Model):
    name = models.CharField(max_length=255)
    length = models.DurationField(help_text='Format: "1 12:00:00" for 1 day and 12 hours')
    refills = models.IntegerField()
    cover = models.ImageField()

    def __str__(self):
        return '{self.name} ({self.length} days, {self.refills} refills)'.format(self=self)


class Race(models.Model):
    # id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    track = models.ForeignKey(Track, on_delete=models.PROTECT)
    price = models.DecimalField(max_digits=24, decimal_places=18)
    racers_needed = models.IntegerField(validators=(validators.MinValueValidator(2), validators.MaxValueValidator(4)))
    create_datetime = models.DateTimeField(auto_now_add=True)
    started = models.BooleanField(default=False)
    start_datetime = models.DateTimeField(null=True, blank=True)
    finished = models.BooleanField(default=False)
    finish_datetime = models.DateTimeField(null=True, blank=True)
    _rankings = JSONField(null=True, blank=True)

    @transaction.atomic
    def start(self):
        # getcontext().prec = 18

        self.started = True
        self.start_datetime = timezone.now()
        self.save()
        for racer in self.racers.all():
            racer.fill_bids_with_percentages(START_PORTFOLIO_SUM)

    @transaction.atomic
    def finish(self):
        rankings_sum = {}
        rankings = []
        winner_racers = []
        for racer in self.racers.all():
            rankings_sum.setdefault(racer.portfolio_sum, []).append(racer)
        for i, key in enumerate(sorted(rankings_sum.keys(), reverse=True)):
            if i == 0:
                winner_racers = rankings_sum[key]
            rankings.append((rankings_sum[key][0].portfolio_diff_str, [racer.pk for racer in rankings_sum[key]]))

        # spread winnings to top racers
        winning = self.price * self.racers_count / len(winner_racers)
        for racer in winner_racers:
            racer.customer.balance += winning
            racer.customer.save()

        self.finished = True
        self.finish_datetime = timezone.now()
        self._rankings = rankings
        self.save()

    @cached_property
    def rankings(self):
        result = []
        for rank, row in enumerate(self._rankings, 1):
            result.append((rank, row[0], [Racer.objects.get(pk=pk) for pk in row[1]]))
        return result

    @cached_property
    def racers_count(self):
        return self.racers.count()

    @cached_property
    def is_ready(self):
        return self.racers_count >= self.racers_needed

    def get_racer_by_customer(self, customer):
        try:
            return self.racers.get(customer=customer)
        except (Racer.DoesNotExist, Racer.MultipleObjectsReturned):
            return None

    def __str__(self):
        return '#{} ({})'.format(self.pk, self.track)


class Racer(models.Model):
    race = models.ForeignKey(Race, on_delete=models.PROTECT, related_name='racers')
    customer = models.ForeignKey(Customer, on_delete=models.PROTECT, related_name='racers')
    bids = JSONField(default={}, editable=False)
    percentages = JSONField(editable=False)
    refills_used = models.IntegerField(default=0)

    def fill_bids_with_percentages(self, total, percentages=None):
        if percentages is None:
            percentages = self.percentages
        else:
            self.percentages = percentages
        bids = {}
        for (code, percent) in dict(percentages).items():
            try:
                currency_price = Currency.objects.get(code=code).price
                bids[code] = str(total * Decimal(percent) / currency_price)
            except Currency.DoesNotExist:
                continue
        self.bids = bids
        self.save()

    @cached_property
    def portfolio_sum(self):
        # getcontext().prec = 18

        result = Decimal(0)
        for code, bid in dict(self.bids).items():
            try:
                currency_price = Currency.objects.get(code=code).price
                result += Decimal(bid) * currency_price
            except Currency.DoesNotExist:
                continue
        return result

    @cached_property
    def portfolio_diff(self):
        return (self.portfolio_sum - START_PORTFOLIO_SUM) / START_PORTFOLIO_SUM

    @cached_property
    def portfolio_diff_str(self):
        return '{:+.2f}'.format(self.portfolio_diff * 100)

    @cached_property
    def bids_list(self):
        # getcontext().prec = 18

        result = []
        for code in self.percentages:
            try:
                currency = Currency.objects.get(code=code)
                bid = Decimal(dict(self.bids).get(code))
                result.append({
                    'currency': currency,
                    'percentage': bid * currency.price / self.portfolio_sum * 100,  # self.portfolio_sum is cached
                    'bid': bid
                })
            except Currency.DoesNotExist:
                continue
        return result

    @cached_property
    def has_refills_left(self):
        return self.refills_used < self.race.track.refills

    def __str__(self):
        return '{} (race {})'.format(self.customer, self.race)
