from django.contrib import admin

from .models import Track, Race, Racer


@admin.register(Track)
class TrackAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'length', 'refills')

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return 'length', 'refills'
        return super(TrackAdmin, self).get_readonly_fields(request, obj)


@admin.register(Race)
class RaceAdmin(admin.ModelAdmin):
    list_display = ('track', 'create_datetime', 'started', 'start_datetime', 'finished', 'finish_datetime',
                    'price', 'racers_needed')
    readonly_fields = ('track', 'create_datetime', 'started', 'start_datetime', 'finished', 'finish_datetime',
                       'price', 'racers_needed', '_rankings')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Racer)
class RacerAdmin(admin.ModelAdmin):
    list_display = ('race', 'customer', 'refills_used')
    readonly_fields = ('race', 'customer', 'refills_used', 'bids', 'percentages')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
