from django.conf.urls import url

from game.views import CreateRaceView, RaceListView, JoinRaceView, WatchRaceView, LeaveRaceView, RefillView, \
    AjaxRaceStateView, SelectSkinView

app_name = 'game'

urlpatterns = [
    url(r'^create/', CreateRaceView.as_view(), name='create'),
    url(r'^$', RaceListView.as_view(), name='list'),
    url(r'^(?P<pk>\d+)/join/$', JoinRaceView.as_view(), name='join'),
    url(r'^(?P<pk>\d+)/leave/$', LeaveRaceView.as_view(), name='leave'),
    url(r'^(?P<pk>\d+)/refill/$', RefillView.as_view(), name='refill'),
    url(r'^(?P<pk>\d+)/state/$', AjaxRaceStateView.as_view(), name='ajax_state'),
    url(r'^(?P<pk>\d+)/skin/$', SelectSkinView.as_view(), name='skin'),
    url(r'^(?P<pk>\d+)/$', WatchRaceView.as_view(), name='watch'),
]
