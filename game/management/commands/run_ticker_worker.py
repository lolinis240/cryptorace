import logging
from random import uniform

import requests
from decimal import Decimal
from django.conf import settings
from django.core.management import BaseCommand

from customer.models import Currency
from tokenfight.utils import WorkerMixin


class Command(WorkerMixin, BaseCommand):
    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)
        self.logger = logging.getLogger(__name__)
        self.work_sleep_time = 5 if settings.DEMO else 50 * 5

    def handle(self, *args, **options):
        self.run()

    def _work(self):
        if settings.DEMO:
            for currency in Currency.objects.all():
                currency.price += Decimal(uniform(-.1, .1))
                if currency.price < .1:
                    currency.price = .1
                currency.save()
        else:
            ticker = requests.get('https://api.coinmarketcap.com/v1/ticker/?limit=0').json()
            ticker = {coin['symbol']: coin for coin in ticker}

            for currency in Currency.objects.all():
                try:
                    currency.price = ticker[currency.code.upper()]['price_usd']
                    currency.save()
                except KeyError:
                    pass
