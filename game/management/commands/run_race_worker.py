import logging

from django.core.management import BaseCommand
from django.db.models import F, ExpressionWrapper, DateTimeField
from django.utils import timezone

from game.models import Race
from tokenfight.utils import ModelWorkerMixin


class Command(ModelWorkerMixin, BaseCommand):
    work_sleep_time = 1

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)
        self.logger = logging.getLogger(__name__)

    def handle(self, *args, **options):
        self.run()

    def get_queryset(self):
        # annotate queryset with estimated end datetime
        return Race.objects\
            .filter(started=True, finished=False)\
            .annotate(end=ExpressionWrapper(F('start_datetime') + F('track__length'), output_field=DateTimeField()))\
            .filter(end__lte=timezone.now())

    def _work_item(self, race):
        race.finish()
