from decimal import Decimal, getcontext

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import JsonResponse
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, DetailView, ListView
from django.views.generic.edit import FormMixin, DeleteView, FormView, UpdateView

from customer.forms import CarSkinForm
from customer.models import Currency, Customer, CarSkin
from .forms import JoinRaceForm
from .models import Race, Track, Racer


class CreateRaceView(LoginRequiredMixin, CreateView):
    model = Race
    template_name = 'game/create_race.html'
    fields = ('track', 'racers_needed', 'price',)

    def get_success_url(self):
        return reverse('game:skin', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        ctx = super(CreateRaceView, self).get_context_data(**kwargs)
        ctx['tracks'] = Track.objects.all()
        return ctx

    def form_valid(self, form):
        result = super(CreateRaceView, self).form_valid(form)
        if settings.DEMO and self.request.GET.get('demo') is not None:
            percentages = {c.code: (1 if i == 0 else 0) for i, c in enumerate(Currency.objects.order_by('?'))}
            Racer.objects.create(race=self.object,
                                 customer=Customer.objects.exclude(user_id=self.request.user)
                                                          .filter(balance__gte=self.object.price).order_by('?').first(),
                                 percentages=percentages)
        return result


class RaceListView(ListView):
    model = Race
    template_name = 'game/race_list.html'
    queryset = Race.objects.all()
    paginate_by = 12

    def get_context_data(self, *args, **kwargs):
        ctx = super(RaceListView, self).get_context_data(*args, **kwargs)
        ctx['not_started'] = Race.objects.filter(started=False, finished=False)
        ctx['ongoing'] = Race.objects.filter(started=True, finished=False)
        if self.request.user.is_authenticated:
            ctx['joined_races'] = self.request.user.customer.racers.values_list('race__pk', flat=True)
        else:
            ctx['joined_races'] = []
        return ctx


class JoinRaceView(FormMixin, DetailView):
    model = Race
    template_name = 'game/join_race.html'
    queryset = Race.objects.all()
    form_class = JoinRaceForm

    def dispatch(self, request, *args, **kwargs):
        race = self.get_object()
        error = False

        if request.user.is_authenticated:
            if race.get_racer_by_customer(request.user.customer):
                messages.warning(request, 'You are already participating in this race')
                error = True
            elif race.finished:
                messages.warning(request, 'You cannot join this race anymore because it has already finished')
                error = True
            elif race.started:
                messages.warning(request, 'You cannot join this race anymore because it has already started')
                error = True
            elif request.user.customer.balance < race.price:
                messages.warning(request, 'Balance is not enough')
                error = True

            if not error:
                return super(JoinRaceView, self).dispatch(request, *args, **kwargs)
        else:
            messages.warning(request, 'You are not logged in')

        return redirect('game:watch', pk=race.pk)

    def get_success_url(self):
        return reverse('game:watch', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        ctx = super(JoinRaceView, self).get_context_data(**kwargs)
        ctx['currencies'] = Currency.objects.all()
        return ctx

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        # getcontext().prec = 18

        with transaction.atomic():
            Racer.objects.create(race=self.object,
                                 customer=self.request.user.customer,
                                 percentages=form.cleaned_data['percentages'])

            # subtract participation fee
            self.request.user.customer.balance -= self.object.price
            self.request.user.customer.save()

            if self.object.is_ready:
                self.object.start()

        messages.success(self.request, 'You have joined this race')

        return super(JoinRaceView, self).form_valid(form)


class RefillView(FormMixin, DetailView):
    model = Race
    template_name = 'game/join_race.html'
    queryset = Race.objects.all()
    form_class = JoinRaceForm

    def dispatch(self, request, *args, **kwargs):
        race = self.get_object()
        error = False

        if request.user.is_authenticated:
            racer = race.get_racer_by_customer(request.user.customer)
            if not racer:
                messages.warning(request, 'You are not participating in this race')
                error = True
            elif race.finished:
                messages.warning(request, 'You cannot refill in this race anymore because it has already finished')
                error = True
            elif not race.started:
                messages.warning(request, 'You cannot refill in this race because it has not yet started')
                error = True
            elif racer.refills_used >= race.track.refills:
                messages.warning(request, 'You have no more refills left')
                error = True

            if not error:
                return super(RefillView, self).dispatch(request, *args, **kwargs)
        else:
            messages.warning(request, 'You are not logged in')

        return redirect('game:watch', pk=race.pk)

    def get_success_url(self):
        return reverse('game:watch', kwargs={'pk': self.object.pk})

    def get_context_data(self, **kwargs):
        ctx = super(RefillView, self).get_context_data(**kwargs)
        ctx['currencies'] = Currency.objects.all()
        return ctx

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        # getcontext().prec = 18

        with transaction.atomic():
            racer = self.object.get_racer_by_customer(self.request.user.customer)
            racer.refills_used += 1  # racer gets saved in the next line call
            racer.fill_bids_with_percentages(racer.portfolio_sum, form.cleaned_data['percentages'])

        messages.success(self.request, 'You have refilled your car')

        return super(RefillView, self).form_valid(form)


class LeaveRaceView(DetailView):
    model = Race
    queryset = Race.objects.all()
    template_name = 'game/leave_race.html'

    def dispatch(self, request, *args, **kwargs):
        race = self.get_object()
        error = False

        if self.request.user.is_authenticated:
            if not race.get_racer_by_customer(request.user.customer):
                messages.warning(request, 'You are not participating in this race')
                error = True
            elif race.finished:
                messages.warning(request, 'You cannot leave this race anymore because it has already finished')
                error = True
            elif race.started:
                messages.warning(request, 'You cannot leave this race anymore because it has already started')
                error = True

            if not error:
                return super(LeaveRaceView, self).dispatch(request, *args, **kwargs)
        else:
            messages.warning(request, 'You are not logged in')

        return redirect('game:watch', pk=race.pk)

    def post(self, request, *args, **kwargs):
        # getcontext().prec = 18

        race = self.get_object()
        with transaction.atomic():
            request.user.customer.balance += race.price
            request.user.customer.save()
            race.get_racer_by_customer(request.user.customer).delete()

        messages.success(request, 'You have left this race')
        return redirect('game:watch', pk=race.pk)


class WatchRaceView(DetailView):
    model = Race
    template_name = 'game/watch_race.html'
    queryset = Race.objects.all()
    context_object_name = 'race'

    def get_context_data(self, **kwargs):
        ctx = super(WatchRaceView, self).get_context_data(**kwargs)
        ctx['currencies'] = Currency.objects.all()
        ctx['skins'] = CarSkin.objects.all()
        ctx['racers'] = sorted(self.object.racers.all(), key=lambda racer: racer.portfolio_sum, reverse=True)

        if self.request.user.is_authenticated:
            ctx['current_racer'] = self.object.get_racer_by_customer(self.request.user.customer)
        else:
            ctx['current_racer'] = None

        return ctx


class AjaxRaceStateView(DetailView):
    model = Race
    queryset = Race.objects.all()

    def get(self, request, *args, **kwargs):
        race = self.get_object()
        racers_data = []
        extra = {}
        if race.started and race.finished:
            for rank, diff, racers in race.rankings:
                for racer in racers:
                    racers_data.append({
                        'rank': rank,
                        'rank_suffix': {1: 'st.', 2: 'nd.', 3: 'rd.'}.get(rank, 'th.'),
                        'diff': diff,
                        'racer': {
                            'id': racer.id,
                            'username': racer.customer.username,
                            'avatar': racer.customer.avatar.url,
                            'car_skin': racer.customer.car_skin.pk,
                        }
                    })
        elif race.started and not race.finished:
            for i, racer in enumerate(sorted(race.racers.all(), key=lambda r: r.portfolio_sum, reverse=True), 1):
                racers_data.append({
                    'rank': i,
                    'rank_suffix': {1: 'st.', 2: 'nd.', 3: 'rd.'}.get(i, 'th.'),
                    'racer': {
                        'id': racer.id,
                        'username': racer.customer.username,
                        'avatar': racer.customer.avatar.url,
                        'car_skin': racer.customer.car_skin.pk,
                        'diff_str': racer.portfolio_diff_str,
                        'bids_list': [{
                            'percentage': row['percentage'],
                            'currency_code': row['currency'].code
                        } for row in racer.bids_list],
                    }
                })
        elif not race.started and not race.finished:
            for i, racer in enumerate(race.racers.all(), 1):
                racers_data.append({
                    'rank': i,
                    'rank_suffix': {1: 'st.', 2: 'nd.', 3: 'rd.'}.get(i, 'th.'),
                    'racer': {
                        'id': racer.id,
                        'username': racer.customer.username,
                        'avatar': racer.customer.avatar.url,
                        'car_skin': racer.customer.car_skin.pk,
                    }
                })
        return JsonResponse({
            'started': race.started,
            'finished': race.finished,
            'racers': race.racers_count,
            'racers_data': racers_data,
            'extra': extra,
            'start_datetime': None if not race.start_datetime else race.start_datetime.timestamp()
        })


class SelectSkinView(LoginRequiredMixin, UpdateView):
    form_class = CarSkinForm
    template_name = 'game/select_skin.html'

    def get_success_url(self):
        return reverse('game:join', kwargs=self.kwargs)

    def get_object(self, queryset=None):
        return self.request.user.customer
