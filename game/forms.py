import json

from decimal import Decimal, getcontext
from django import forms
from django.core.exceptions import ValidationError

from customer.models import Currency


class JoinRaceForm(forms.Form):
    percentages = forms.CharField(widget=forms.Textarea())

    def clean_percentages(self):
        # getcontext().prec = 2

        try:
            percentages = json.loads(self.cleaned_data['percentages'])
        except json.JSONDecodeError:
            raise ValidationError('Couldn\'t decode JSON')

        currencies = {c.code for c in Currency.objects.all()}
        if currencies.difference(percentages.keys()):
            raise ValidationError('Wrong currencies list, required: [{}]'.format(', '.join(currencies)))

        values = [Decimal(bid) for bid in percentages.values()]

        if sum(values) != Decimal(1):
            raise ValidationError('Bids sum is not equal 1')

        if any(value < 0 for value in values):
            raise ValidationError('Bids cannot be less than 0')

        return {code: str(Decimal(bid)) for (code, bid) in percentages.items()}
