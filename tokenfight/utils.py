from time import sleep

from django.conf import settings


class WorkerMixin(object):
    logger = None
    object_iteration_limit = 100
    work_sleep_time = 10

    def run(self):
        while 1:
            try:
                self._work()
            except KeyboardInterrupt:
                self.logger.info(u'Interrupting...')
                break
            except Exception as e:
                self.logger.error(u'%s, msg: %s' % (type(e), e))

            sleep(self.work_sleep_time)

    def _work(self):
        raise NotImplementedError()


class ModelWorkerMixin(WorkerMixin):
    iteration_sleep_time = 0

    def _work(self):
        queryset = self.get_queryset()

        cnt = queryset.count()

        if cnt:
            self.logger.debug(u'Total objects count need work %s', cnt)

        if self.object_iteration_limit:
            queryset = queryset[:self.object_iteration_limit]

        for item in queryset:
            try:
                self.logger.debug(u'Start work on item "%s"', item)
                self._work_item(item)
                self.logger.debug(u'Finished work on item "%s"', item)
            except Exception as e:
                self.logger.error(u'_work_item %s, msg: %s' % (type(e), e))

            sleep(self.iteration_sleep_time)

    def _work_item(self, item):
        raise NotImplementedError()

    def get_queryset(self):
        raise NotImplementedError()


def context_processor(request):
    return {
        'DEMO': settings.DEMO,
    }
