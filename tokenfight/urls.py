from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^customer/', include('customer.urls', namespace='customer')),
    url(r'^game/', include('game.urls', namespace='game')),

    url(r'^', include('front.urls', namespace='front')),
]

admin.site.site_title = 'CryptoRace.io admin'
admin.site.site_header = 'CryptoRace.io administration'

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

